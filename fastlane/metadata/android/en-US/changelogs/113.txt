Bug fixes
- #831: Version 4.1.1 and develop: 'jukebox on/off' no longer shown in 'Now Playing'.

Enhancements
- #827: Make app full compliant Android Auto to publish in Play Store.
- #878: "Play shuffled" option for playlists always begins with the first track.
- #891: Dump config to log file when logging is enabled.
